<div align="center">
<picture>
  <source media="(prefers-color-schema: dark)" srcset="/logo/marmela-blue-light.svg">
  <source media="(prefers-color-schema: light)" srcset="/logo/marmela-blue-dark.svg">
  <img alt="marmela" src="/logo/marmela-blue-dark.svg">
</picture>
</div>
